import sys
import csv
import subprocess as sp
from typing import Dict, Tuple


BalancesType = Dict[str, Tuple[float, str]]


def read_ledger_balances(filename: str) -> BalancesType:
    proc = sp.run(["ledger", "-f", filename, "balance", "--unround"],
                  stdout=sp.PIPE, stderr=sp.PIPE, encoding="utf-8")
    if proc.returncode or proc.stderr:
        print("Error running ledger command")
        print(proc.stderr.strip())
        sys.exit(1)

    output = proc.stdout
    balances: BalancesType = dict()
    for line in output.splitlines():
        parts = line.split(maxsplit=2)
        if len(parts) < 3:
            continue
        valuestr, currency, account = parts
        value = float(valuestr)
        balances[account] = (value, currency)

    return balances


def read_csv_balances(filename: str) -> BalancesType:
    currency = "EUR"
    balances: BalancesType = dict()
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        csvdata = list(reader)

    for row in csvdata:
        if row[0] == "Account":  # header
            continue
        account = row[0].strip()
        valuestr = row[-1]
        try:
            value = float(valuestr)
        except ValueError:
            # not a value row
            continue
        balances[account] = (value, currency)

    return balances


def check_matching_balances(ledgerbals: BalancesType, csvbals: BalancesType):
    nmissing = 0
    nmismatch = 0
    for account, ledger_value_currency in ledgerbals.items():
        if ":" in account:
            account = account.split(":")[-1]
        csv_value_currency = csvbals.get(account, None)
        if csv_value_currency is None:
            print(f"Account '{account}' not found in CSV files")
            nmissing += 1
            continue

        csv_value = csv_value_currency[0]
        ledger_value = ledger_value_currency[0]
        if abs(ledger_value) != abs(csv_value):
            print(f"Value mismatch for '{account}':")
            print(f"CSV:    {csv_value}")
            print(f"Ledger: {ledger_value}")
            nmismatch += 1

    for account in csvbals:
        if account not in ledgerbals:
            print(f"Account '{account}' not found in ledger file")
            nmissing += 1

    print("-------------------")
    print(f"{nmissing} missing")
    print(f"{nmismatch} mismatching values")
    nledger = len(ledgerbals)
    ncsv = len(csvbals)
    print(f"{nledger} ledger accounts")
    print(f"{ncsv} csv accounts")


def main():
    if len(sys.argv) < 3:
        print(f"Usage: {sys.argv[0]} <ledgerfile> <csvfiles>...")
        sys.exit(1)

    ledgerfn = sys.argv[1]
    csvfns = sys.argv[2:]

    print(f"Validating ledger file {ledgerfn}")
    print(f"Using {', '.join(csvfns)}")

    ledger_balances = read_ledger_balances(ledgerfn)
    csv_balances: BalancesType = dict()
    for csvfn in csvfns:
        csv_balances.update(read_csv_balances(csvfn))

    check_matching_balances(ledger_balances, csv_balances)


if __name__ == "__main__":
    main()
