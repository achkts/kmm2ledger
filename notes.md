# KMyMoney to Ledger conversion

## Ledger format

The main block in a Ledger file describes a transaction.

In its simplest form, a Ledger transaction has a date, a payee, and a list of entries that consist of account-value pairs.
```
<DATE> <PAYEE>
  <ACCOUNT 1>            <VALUE 1>
  <ACCOUNT 2>            <VALUE 2>
  ...
  <ACCOUNT N>            <VALUE N>
```

- The sum of all VALUEs must be 0.
- There can be any number of `<ACCOUNT> <VALUE>` pairs.
- In simple cases, the balancing value can be omitted.

In Ledger, *Account* is used as a general term to describe both accounts and (what other programs call) categories.

Transactions can be enhanced with tags and comments.  Tags can also be used on individual entries to specify multiple payees for a single transaction (e.g., a single payment to multiple expense categories).
```
<DATE> <PAYEE>
  ; <MEMO COMMENT>
  <ACCOUNT 1>            <VALUE 1>  ; <PAYEE TAG>
  <ACCOUNT 2>            <VALUE 2>
  ...
  <ACCOUNT N>            <VALUE N>
  ; :<TAG 1>:<TAG 2>:...:<TAG N>:
```

- `<MEMO COMMENT>` is used to attach the memo to a transaction.  Each line in a multiline comment/memo must be prefixed with a semicolon.
- `<PAYEE TAG>` is of the form `Payee:<name>` to specify split payees.
- The last line comment specifies tags, which should have no spaces in their names and must be surrounded in and separated by colons.

## KMyMoney format

KMyMoney stores its data in an XML file.  The XML tree has a grouping (root) element for each type of record and child elements are instances of that record type.  The following record types are of interest:

- ACCOUNT: A bank or card account or an income/expense category
    - type
    - lastreconciled
    - description
    - name
    - lastmodified
    - parentaccount
    - number
    - id
    - currency
    - institution
    - opened
- TAG
    - name
    - tagcolor
    - id
    - closed
- TRANSACTION: A transaction which defines a commodity (currency), date, and memo.  The values in the transaction are contained in a subelement rooted at SPLITS which itself contain multiple SPLIT records.  A transaction can also contain multiple TAG child elements.
    - commodity
    - postdate
    - entrydate
    - id
    - memo
- SPLIT
    - reconciledate
    - action
    - price
    - bankid
    - account
    - payee
    - reconcileflag
    - number
    - id
    - value
    - shares
    - memo
- PAYEE
    - matchingenabled
    - name
    - matchignorecase
    - matchkey
    - id
    - reference
    - email
    - usingmatchkey

The following objects might be useful later:
- REPORT
    - includesmovingaverage
    - datelock
    - includeschedules
    - skipZero
    - rowtype
    - columntype
    - investments
    - group
    - columnsaredays
    - type
    - includesforecast
    - showrowtotals
    - includesaverageprice
    - mixedtime
    - includesprice
    - chartdatalabels
    - chartbydefault
    - includestransfers
    - includesactuals
    - chartchgridlines
    - charttype
    - chartlinewidth
    - favorite
    - logYaxis
    - dataRangeEnd
    - yLabelsPrecision
    - chartsvgridlines
    - convertcurrency
    - id
    - datalock
    - dataMajorTick
    - dataRangeStart
    - includeunused
    - detail
    - name
    - dataMinorTick
    - showcolumntotals
    - comment
- SCHEDULED_TX
    - endDate
    - type
    - name
    - fixed
    - weekendOption
    - occurence
    - paymentType
    - occurenceMultiplier
    - lastPayment
    - startDate
    - id
    - lastDayInMonth
    - autoEnter
