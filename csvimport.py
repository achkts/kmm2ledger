import sys
import csv
from typing import Dict, Tuple, List
from datetime import datetime
from decimal import Decimal


TransactionListType = List[Tuple[datetime, str, Decimal]]


def parse_csv(filename: str) -> TransactionListType:
    transactions: TransactionListType = list()
    with open(filename, encoding="utf-8") as csvfile:
        csvdata = csv.reader(csvfile, delimiter=";")
        for idx, row in enumerate(csvdata):
            if idx < 5:
                # skip header stuff
                continue
            if row[0] in ("Balance:", "Online-Balance:"):
                # skip last line showing balance
                continue
            datestr, _, description, *_, valuestr, currency = row
            value = Decimal(valuestr.replace("- ", "-"))
            date = datetime.strptime(datestr, "%m/%d/%Y")
            transactions.append((date, description, value))
    return transactions


def main():
    if len(sys.argv) < 3:
        print(f"Usage: {sys.argv[0]} <ledgerfile> <csvfiles>...")
        sys.exit(1)

    ledgerfn = sys.argv[1]
    csvfns = sys.argv[2:]

    print(f"Ledger file {ledgerfn}")
    if len(csvfns) > 3:
        print(f":: Importing {len(csvfns)} CSV files")
    else:
        print(f":: Importing {', '.join(csvfns)}")

    transactions: TransactionListType = list()
    for fn in csvfns:
        transactions.extend(parse_csv(fn))

    total = Decimal(0)
    for trans in sorted(set(transactions)):
        count = transactions.count(trans)
        if count > 1:
            print(f"{trans} found {count} times")
        total += trans[2]
    print(f"Total: {total}")


if __name__ == "__main__":
    main()
