import os
import sys
import gzip
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element as XMLElement
from typing import Dict, List, Any, Iterable


class Account(object):
    """
    Represents an Account (or category) from a KMyMoney file with all
    associated data.  Does not link in any intelligent way to parent accounts,
    but the ID of the parent account is stored.
    """

    def __init__(self, element: XMLElement):
        if element.tag != "ACCOUNT":
            raise TypeError("Invalid XML Element received for Account: "
                            f"{element.tag}")
        attr = element.attrib
        self.id = attr["id"]
        self.type = attr["type"]
        self.lastreconciled = attr["lastreconciled"]
        self.description = attr["description"]
        self.name = attr["name"]
        self.lastmodified = attr["lastmodified"]
        self.parentaccount = attr["parentaccount"]
        self.number = attr["number"]
        self.currency = attr["currency"]
        self.institution = attr["institution"]
        self.opened = attr["opened"]

    def full_name(self, accounts: Dict[str, Any]) -> str:
        """
        Returns the name of the Account with parent names prefixed (separated
        by : colon).  This is how Ledger handles hierarchical accounts.
        """
        parentstr = ""
        if self.parentaccount:
            parentstr = accounts[self.parentaccount].full_name(accounts) + ":"
        return parentstr + self.name

    def __str__(self):
        return f"<Account {self.name} ({self.id})>"

    def __repr__(self):
        return str(self)


class Transaction(object):
    """
    Represents a single Transaction from a KMyMoney file with all associated
    data.  Contains a dictionary `.splits` which references all the splits
    that make up the transaction, indexed by their ID.
    """

    def __init__(self, element: XMLElement):
        if element.tag != "TRANSACTION":
            raise TypeError("Invalid XML Element received for Transaction: "
                            f"{element.tag}")
        attr = element.attrib
        self.commodity = attr["commodity"]
        self.postdate = attr["postdate"]
        self.entrydate = attr["entrydate"]
        self.id = attr["id"]
        self.memo = attr["memo"]
        self.splits: Dict[str, Split] = dict()
        splitsel = element.find("SPLITS")
        if splitsel:
            for splitel in splitsel:
                split = Split(splitel)
                self.splits[split.id] = split

    def __str__(self):
        return f"<Transaction {self.id} [{len(self.splits)} splits]>"

    def __repr__(self):
        return str(self)


class Tag(object):
    """
    Represents a single Tag from a KMyMoney file with all associated data.
    """

    def __init__(self, element: XMLElement):
        if element.tag != "TAG":
            raise TypeError("Invalid XML Element received for Tag: "
                            f"{element.tag}")
        attr = element.attrib
        self.id = attr["id"]
        self.name = attr["name"]
        self.tagcolor = attr["tagcolor"]
        self.closed = attr["closed"]

    def __str__(self):
        return f"<Tag {self.name} ({self.id})"

    def __repr__(self):
        return str(self)


class Payee(object):
    """
    Represents a single Tag from a KMyMoney file with all associated data.
    """

    def __init__(self, element: XMLElement):
        if element.tag != "PAYEE":
            raise TypeError("Invalid XML Element received for Payee: "
                            f"{element.tag}")
        attr = element.attrib
        self.id = attr["id"]
        self.matchingenabled = attr["matchingenabled"]
        self.name = attr["name"]
        self.reference = attr["reference"]
        self.email = attr["email"]
        self.matchignorecase = attr.get("matchignorecase", "0")
        self.matchkey = attr.get("matchkey", "")
        self.usingmatchkey = attr.get("usingmatchkey", "0")

    def __str__(self):
        return f"<Payee {self.name} ({self.id})>"

    def __repr__(self):
        return str(self)


class Split(object):

    def __init__(self, element: XMLElement):
        if element.tag != "SPLIT":
            raise TypeError("Invalid XML Element received for Split: "
                            f"{element.tag}")
        attr = element.attrib
        self.reconciledate = attr["reconciledate"]
        self.action = attr["action"]
        self.price = attr.get("price", "0")
        self.bankid = attr["bankid"]
        self.account = attr["account"]
        self.payee = attr["payee"]
        self.reconcileflag = attr["reconcileflag"]
        self.number = attr["number"]
        self.id = attr["id"]
        self.value = attr["value"]
        self.shares = attr["shares"]
        self.memo = attr["memo"]
        self.tags = [tag.attrib["id"] for tag in element.findall("TAG")]

    def __str__(self):
        return f"<Split {self.id} (Value {self.value} Price {self.price})"

    def __repr__(self):
        return str(self)


class LedgerEntry(object):
    """
    Represents a single transaction with information required to generate an
    entry in the format for Ledger (https://www.ledger-cli.org/).  For
    instantiation, it requires supplying a KMyMoney Transaction and all
    Accounts, Payees, and Tags it might reference.  To ease multiple
    instantiations, see the `LedgerEntryMaker` class.

    The string representation of LedgerEntry instances are in the format
    supported by Ledger.
    """

    def __init__(self, transaction: Transaction,
                 accounts: Dict[str, Account],
                 payees: Dict[str, Payee],
                 tags: Dict[str, Tag]):
        self.date = transaction.postdate
        payeeids = [s.payee for s in transaction.splits.values()]
        payeenames = [payees[pid].name if pid else "" for pid in payeeids]
        self.payees = payeenames
        accountids = [s.account for s in transaction.splits.values()]
        # KMM uses the value attribute to store the base value of the
        # transaction and the shares attribute to store the value of the
        # transaction in its given currency (or commodity)
        values = [s.shares for s in transaction.splits.values()]
        accountnames = [accounts[aid].full_name(accounts)
                        for aid in accountids]
        commodities = [accounts[aid].currency for aid in accountids]
        self.entries = list(zip(accountnames, values, commodities))

        tagnames: List[str] = list()
        for split in transaction.splits.values():
            tagids = split.tags
            # Ledger doesn't support tags with spaces: converting to dash
            splittagnames = [tags[tid].name.replace(" ", "-")
                             for tid in tagids]
            tagnames.extend(splittagnames)

        self.tags = list(set(tagnames))
        # Use comments to store memo
        self.comments = transaction.memo

    def __str__(self):
        # Use first split payee name for transaction (assuming first is
        # primary).  Might be worth revisiting.
        # Asterisk marks the transaction "Cleared"
        payeeline = f"{self.date} * {self.payees[0]}"

        # prepend comment character to each comment line
        commentlines = [f"; {line}" for line in self.comments.splitlines()]

        # KMM stores values as expressions (e.g., 10/1 for 10 EUR or 52/100
        # for 52 cents).  Wrapping the value in parentheses allows Ledger to
        # parse these expressions properly and doesn't require us to evaluate
        # the expression.
        entrylines: List[str] = list()
        for entry in self.entries:
            account, value, commodity = entry
            valuestr = f"({value} {commodity})"
            entrylines.append(f"{account:<50} {valuestr:>15}")
        payees_filtered = [p for p in self.payees if p]
        if len(set(payees_filtered)) > 1:
            for idx, payee in enumerate(self.payees):
                entrylines[idx] += f" ; Payee: {payee}"
        nl = "\n    "
        entrystr = payeeline + nl
        if commentlines:
            entrystr += nl.join(commentlines) + nl
        entrystr += nl.join(entrylines)
        if self.tags:
            entrystr += nl + "; :" + ":".join(self.tags) + ":"
        return entrystr

    def __repr__(self):
        return f"<LedgerEntry {self.payee} ({self.date}) {self.entries}>"


class LedgerEntryMaker(object):
    """
    Class for generating LedgerEntry objects.  Initialise with dictionaries
    for Account, Payee, and Tag data to make generating multiple LedgerEntry
    objects from the same database easier.
    """

    def __init__(self, accounts: Dict[str, Account],
                 payees: Dict[str, Payee],
                 tags: Dict[str, Tag]):
        self._accounts = accounts
        self._payees = payees
        self._tags = tags

    def make(self, transaction: Transaction) -> LedgerEntry:
        """
        Combines a Transaction's data with the appropriate Account, Payee, and
        Tag data and returns a LedgerEntry with all information required to
        print a Ledger entry.
        """
        return LedgerEntry(transaction,
                           self._accounts, self._payees, self._tags)

    def make_all(self, transactions: Iterable[Transaction])\
            -> List[LedgerEntry]:
        """
        Makes multiple calls to LedgerEntryMaker.make() and returns a list of
        LedgerEntry objects.
        """
        # TODO: Add sorting options
        dicts = (self._accounts, self._payees, self._tags)
        return [LedgerEntry(t, *dicts) for t in transactions]


def readfile(fname: str) -> XMLElement:
    gzfile = gzip.open(fname, "r")
    return ET.fromstring(gzfile.read())


# Note on load_*() functions:
# find() returns a single Element matching the given tag.  KMM groups record
# types under a single element tagged with the plural name of the type (e.g.,
# ACCOUNTS).  We find the first element matching this name (there should only
# be one) and then iterate over it to get the records and convert them.

def load_accounts(kmroot: XMLElement) -> Dict[str, Account]:
    accounts: Dict[str, Account] = dict()
    accountsroot = kmroot.find("ACCOUNTS")
    if not accountsroot:
        return accounts
    for accel in accountsroot:
        acc = Account(accel)
        accounts[acc.id] = acc
    return accounts


def load_payees(kmroot: XMLElement) -> Dict[str, Payee]:
    payees: Dict[str, Payee] = dict()
    payeesroot = kmroot.find("PAYEES")
    if not payeesroot:
        return payees
    for payel in payeesroot:
        payee = Payee(payel)
        payees[payee.id] = payee
    return payees


def load_tags(kmroot: XMLElement) -> Dict[str, Tag]:
    tags: Dict[str, Tag] = dict()
    tagsroot = kmroot.find("TAGS")
    if not tagsroot:
        return tags
    for tagel in tagsroot:
        tag = Tag(tagel)
        tags[tag.id] = tag
    return tags


def load_transactions(kmroot: XMLElement) -> Dict[str, Transaction]:
    transactions: Dict[str, Transaction] = dict()
    transroot = kmroot.find("TRANSACTIONS")
    if not transroot:
        return transactions
    for transel in transroot:
        trans = Transaction(transel)
        transactions[trans.id] = trans
    return transactions


def main():
    if len(sys.argv) < 2:
        print("Missing required argument: KMyMoney filename (.kmy)")
        sys.exit(1)
    if len(sys.argv) > 2:
        print("Too many arguments. Provide only one KMyMoney filename (.kmy)")
        sys.exit(1)

    filename = sys.argv[1]

    print("Loading...", end=" ", flush=True)
    kmroot = readfile(filename)
    accounts = load_accounts(kmroot)
    payees = load_payees(kmroot)
    tags = load_tags(kmroot)
    print("done")

    lem = LedgerEntryMaker(accounts, payees, tags)

    transactions = load_transactions(kmroot)

    print(f"  {len(accounts)} accounts")
    print(f"  {len(payees)} payees")
    print(f"  {len(tags)} tags")
    print(f"  {len(transactions)} transactions")

    print("Converting to ledger...", end=" ", flush=True)
    ledger_transactions = lem.make_all(transactions.values())
    print("done")
    print(f"Successfully converted {len(ledger_transactions)} transactions")

    outfilename, _ = os.path.splitext(filename)
    outfilename += ".dat"
    if os.path.exists(outfilename):
        print(f"Output file {outfilename} already exists")
        confirm = ""
        while confirm not in ("y", "n"):
            confirm = input("Overwrite? [yn] ")
        if confirm == "n":
            print("Exiting")
            sys.exit(0)
    print(f"Saving to {outfilename}...", end=" ", flush=True)
    with open(outfilename, "w") as outfile:
        for lt in ledger_transactions:
            outfile.write(str(lt) + "\n\n")
    print("done")


if __name__ == "__main__":
    main()
